<?php 

 use Letter\Collector\LetterCollector; 


LetterCollector::add( 'Notifier.send_notifies', [
  'title' => 'Envío de notificaciones',
  'subject' => [
    'spa' => 'Tienes nuevas notificaciones en {web_name}'
  ],
  'file' => 'send_notifies',
  'vars' => [
    'web_name' => 'Nombre del web',
    'user_name' => 'Nombre del usuario',
    'notifies' => 'Las notificaciones',
  ]
]);
