<?php
namespace Notifier\Test\TestCase\Mailer;

use Cake\TestSuite\TestCase;
use Notifier\Mailer\NotifiesMailer;

/**
 * Notifier\Mailer\NotifiesMailer Test Case
 */
class NotifiesMailerTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Notifier\Mailer\NotifiesMailer
     */
    public $Notifies;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
