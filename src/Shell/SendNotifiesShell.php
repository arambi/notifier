<?php
namespace Notifier\Shell;

use Cake\Console\Shell;
use Notifier\Model\Table\NotificationsTable;
use Letter\Mailer\MailerAwareTrait;

/**
 * SendNotifies shell command.
 */
class SendNotifiesShell extends Shell
{
  use MailerAwareTrait;

  /**
   * Manage the available sub-commands along with their arguments and help
   *
   * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
   *
   * @return \Cake\Console\ConsoleOptionParser
   */
  public function getOptionParser()
  {
      $parser = parent::getOptionParser();

      return $parser;
  }

  /**
   * main() method.
   *
   * @return bool|int|null Success or error code.
   */
  public function main()
  {
    $this->loadModel( 'Notifier.Notifications');

    $user_notifies = $this->Notifications->find()
      ->where([
        'state' => NotificationsTable::STATUS_NO_READED,
        'user_id IS NOT NULL'
      ])
      ->contain([
        'Users'
      ])
      ->group( 'user_id');
    
    foreach( $user_notifies as $user_notify)
    {
      $notifies = $this->Notifications->find()
        ->where([
          'state' => NotificationsTable::STATUS_NO_READED,
          'user_id' => $user_notify->user_id
        ]);
      
      $this->getMailer( 'Notifier.Notifies')->send( 'sendNotifies', [
        $user_notify->user, $notifies
      ]);
    }

  }
}
