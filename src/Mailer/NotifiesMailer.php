<?php
namespace Notifier\Mailer;

use ArrayObject;
use Cake\Routing\Router;
use Website\Lib\Website;
use EmailQueue\EmailQueue;
use Letter\Mailer\LetterTrait;
use Letter\Mailer\QueueMailer;
use Letter\Collector\LetterCollector;

/**
 * Notifies mailer.
 */
class NotifiesMailer extends QueueMailer
{
  use LetterTrait;

  /**
   * Mailer's name.
   *
   * @var string
   */
  static public $name = 'Notifies';



  public function sendNotifies( $user, $notifies)
  {
    $site = $this->loadModel( 'Website.Sites')->find()->first();
    Website::set( $site);
    $notifications = new ArrayObject();
    
    foreach( $notifies as $notify)
    {
      $text = '<div class="c-notify-email__type">'. $notify->category .'</div>';
      $text .= '<div class="c-notify-email__title">'. $notify->title .'</div>';
  
      if( !empty( $notify->link))
      {
        $text .= ' <a href="'. $notify->link .'">'. __d( 'app', 'Enlace') .'</a>';
      }
  
      $notifications [] = '<div class="c-notify-email">'. $text . '</div>';
    }

    $notifications = implode( "\n", (array)$notifications);
    $this->letter = LetterCollector::get( 'Notifier.send_notifies', [
      'notifies' => $notifications,
      'web_name' => Website::get( 'title'),
      'user_name' => $user->name
    ]);

    $this->_setLetterParams();
    $this->_email->to( $user->email);
    $this->_email->from( Website::get( 'settings.users_reply_email'));
  }
}
